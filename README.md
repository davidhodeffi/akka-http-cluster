akka-docker-cluster-example
===========================

An example akka-http-cluster project with docker support.  Uses [SBT Native Packager](https://github.com/sbt/sbt-native-packager). Uses sbt multi-jvm plugin

### How to Run

In SBT, just run `docker:publishLocal` to create a local docker container. 

To run the cluster, run `docker-compose up`. This will create 3 nodes, a seed and two regular members, called `seed`, `c1`, and `c2` respectively.

While running, try opening a new terminal and (from the same directory) try things like `docker-compose down seed` and watch the cluster nodes respond.

Running tests in multi-jvm command is :'sbt multi-jvm:test'

You should look for good examples at : 'sample.multinode.WebServerSpec.scala'

### Details
This project use `akka-http` and `akka-cluster` and `akka-distibuted-data`.
REST api is exposed on the webserver which puts/get data from backend Actor for a replicated cache. This way our webserver cache is distibuted and shared between all our HTTP servers.
Future work can be integrate the backend cache into an analytic process which can do `something` with the data itself.
The number of Actors which can hold the cache is independent from the number of HTTP server that can run on the cluster.
