package sample.multinode

import akka.actor.{ActorRef, ActorSystem}
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.ddata.Replicator.{GetReplicaCount, ReplicaCount}
import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpRequest, MediaTypes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.remote.testconductor.RoleName
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec}
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.{ByteString, Timeout}
import david.webserver.{ReplicatedCache, RestRoute}
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.japi.function
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestProbe}
import akka.util.{ByteString, Timeout}
import david.webserver.ReplicatedCache.PutInCache
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.io.StdIn
import scala.util.{Failure, Success}


class HttpServerSpecSpecMultiJvmNode1 extends HttpServerSpec

class HttpServerSpecSpecMultiJvmNode2 extends HttpServerSpec

class HttpServerSpecSpecMultiJvmNode3 extends HttpServerSpec

class HttpServerSpec extends MultiNodeSpec(ReplicatedCacheSpec) with STMultiNodeSpec with ImplicitSender {
  import ReplicatedCacheSpec._
  import david.webserver.ReplicatedCache._
  //this may lead to bugs on RouteTest



  override def beforeAll() = multiNodeSpecBeforeAll()

  override def afterAll() = multiNodeSpecAfterAll()

  override def initialParticipants = roles.size

  val cluster = Cluster(system)
  val replicatedCache = system.actorOf(ReplicatedCache.props)
  implicit val materializer = ActorMaterializer()
  val webServer = new TestWebServerServer(replicatedCache)

  def join(from: RoleName, to: RoleName): Unit = {
    runOn(from) {
      cluster join node(to).address
    }
    enterBarrier(from.name + "-joined")
  }

  "Demo of a replicated cache in single http server" must {
    "join cluster" in within(20.seconds) {
      join(node1, node1)
      join(node2, node1)
      join(node3, node1)

      awaitAssert {
        DistributedData(system).replicator ! GetReplicaCount
        expectMsg(ReplicaCount(roles.size))
      }
      enterBarrier("after-1")
    }

    "start restserver" in within(20.seconds) {
      runOn(node1){
          webServer.startServer(80)
      }
      enterBarrier("starting rest server")
    }

    "put data using rest api" in within(10.seconds) {
      val data = s"""
                    |{
                    |    "name":"test"
                    |}
        """.stripMargin

      runOn(node1) {
        val jsonRequest = ByteString(data)
        val postRequest = HttpRequest(
          HttpMethods.POST,
          uri = "http://localhost/post",
          entity = HttpEntity(MediaTypes.`application/json`, jsonRequest))

        val responseFuture: Future[HttpResponse] = Http().singleRequest(postRequest)
        implicit val executionContext = system.dispatcher

        responseFuture
          .onComplete {
            case Success(res) => println("my response:"+res)
            case Failure(x)   => sys.error("something wrong:" +x.getMessage)
          }
      }

      awaitAssert {
        val probe = TestProbe()
        replicatedCache.tell(GetFromCache("key"), probe.ref)
        probe.expectMsg(Cached("key", Some(data)))
      }

      enterBarrier("response got from data")
    }

    "get data using rest api" in within(10.seconds) {
      val data = s"""
                    |{
                    |    "name":"test"
                    |}
        """.stripMargin

      runOn(node1) {
        val jsonRequest = ByteString(data)
        val getRequest = HttpRequest(
          HttpMethods.GET,
          uri = "http://localhost/get",
          entity = HttpEntity(MediaTypes.`application/json`, jsonRequest))

        val responseFuture: Future[HttpResponse] = Http().singleRequest(getRequest)
        implicit val executionContext = system.dispatcher

        responseFuture
          .onComplete {
            case Success(res) =>  res.entity.dataBytes.map(_.utf8String).runForeach(body => body shouldEqual data)
            case Failure(x)   => sys.error("something wrong:" +x.getMessage)
          }
      }

      awaitAssert {
        val probe = TestProbe()
        replicatedCache.tell(GetFromCache("key"), probe.ref)
        probe.expectMsg(Cached("key", Some(data)))
      }



      enterBarrier("response got from data")
    }

    "evict data from different replica should evict from all nodes" in {
      runOn(node2){
        replicatedCache ! Evict("key")
      }

      awaitAssert {
        val probe = TestProbe()
        replicatedCache.tell(GetFromCache("key"), probe.ref)
        probe.expectMsg(Cached("key", None))
      }
      enterBarrier("got data from evicted cache")
    }

    "/get from evicted cache" in within(10.seconds) {
      val data = "{not exists}"

      runOn(node1) {
        val jsonRequest = ByteString(data)
        val getRequest = HttpRequest(
          HttpMethods.GET,
          uri = "http://localhost/get",
          entity = HttpEntity(MediaTypes.`application/json`, jsonRequest))

        val responseFuture: Future[HttpResponse] = Http().singleRequest(getRequest)
        implicit val executionContext = system.dispatcher
        import akka.util.ByteString

        responseFuture
          .onComplete {
            case Success(HttpResponse(StatusCodes.OK, headers, entity, _)) => {
              val responseStr = Unmarshal(entity).to[String]
              responseStr.await shouldEqual  data.stripMargin
            }
            case Failure(x)   => sys.error("something wrong:" +x.getMessage)
          }
      }
      enterBarrier("/get from evicted cache")
    }




    "stop restserver" in within(20.seconds) {
//      runOn(node1){
////        webServer.stopServer()
//      }

      enterBarrier("stopping rest server")
    }

//    "replicate many cached entries" in within(10.seconds) {
//      runOn(node1) {
//        for (i ← 100 to 200)
//          replicatedCache ! PutInCache("key" + i, i)
//      }
//
//      awaitAssert {
//        val probe = TestProbe()
//        for (i ← 100 to 200) {
//          replicatedCache.tell(GetFromCache("key" + i), probe.ref)
//          probe.expectMsg(Cached("key" + i, Some(i)))
//        }
//      }
//
//      enterBarrier("after-3")
//    }
//
//    "replicate evicted entry" in within(15.seconds) {
//      runOn(node1) {
//        replicatedCache ! PutInCache("key2", "B")
//      }
//
//      awaitAssert {
//        val probe = TestProbe()
//        replicatedCache.tell(GetFromCache("key2"), probe.ref)
//        probe.expectMsg(Cached("key2", Some("B")))
//      }
//      enterBarrier("key2-replicated")
//
//      runOn(node3) {
//        replicatedCache ! Evict("key2")
//      }
//
//      awaitAssert {
//        val probe = TestProbe()
//        replicatedCache.tell(GetFromCache("key2"), probe.ref)
//        probe.expectMsg(Cached("key2", None))
//      }
//
//      enterBarrier("after-4")
//    }
//
//    "replicate updated cached entry" in within(10.seconds) {
//      runOn(node2) {
//        replicatedCache ! PutInCache("key1", "A2")
//        replicatedCache ! PutInCache("key1", "A3")
//      }
//
//      awaitAssert {
//        val probe = TestProbe()
//        replicatedCache.tell(GetFromCache("key1"), probe.ref)
//        probe.expectMsg(Cached("key1", Some("A3")))
//      }
//
//      enterBarrier("after-5")
//    }

  }

}

class TestWebServerServer(val replicatedCache: ActorRef)(implicit val system:ActorSystem,
                                                     implicit  val materializer:ActorMaterializer) extends RestRoute{
  var bindingFuture:Future[Http.ServerBinding] = _

  def startServer(port:Int)= {
    assert(Option(bindingFuture).isEmpty,"server already started")
    bindingFuture=Http().bindAndHandle(route, "localhost", port)

  }
  def stopServer() ={
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }


}