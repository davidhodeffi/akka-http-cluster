package david.webserver

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import david.webserver.ReplicatedCache.{Cached, GetFromCache, PutInCache}
import spray.json.DefaultJsonProtocol

import scala.io.StdIn

trait RestRoute extends JsonSupport{
  implicit val system:ActorSystem
  implicit val materializer:ActorMaterializer
  implicit def executionContext = system.dispatcher
  val replicatedCache: ActorRef
  import akka.http.scaladsl.model._
  import akka.pattern.ask
  import scala.concurrent.duration._





  implicit val timeout = Timeout(5 seconds)
  val route =
    logRequestResult("get-rest") {
      path("get") {
        get {
          val response = (replicatedCache ? GetFromCache("key")).mapTo[Cached].map { result =>
            HttpEntity(ContentTypes.`text/html(UTF-8)`, result.value.asInstanceOf[Option[String]].getOrElse("{not exists}") )
          }
          complete(response)
        }
      }
    } ~
      logRequestResult("post-rest") {
        path("post") {
          post {
            entity(as[String]) { posts =>
              replicatedCache ! PutInCache("key",posts)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, s"got ${posts}"))
            }
          }
        }
      }

}
final case class Posts(text: String)


class WebServerServer(val replicatedCache: ActorRef)(implicit val system:ActorSystem,
                                                     implicit  val materializer:ActorMaterializer) extends RestRoute{


  def startServer(address:String, port:Int) = {
    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }


}

object WebServerServer {

  def main(args: Array[String]) {

    implicit val actorSystem = ActorSystem("rest-server")
    implicit val materializer = ActorMaterializer()
    val replicatedCache: ActorRef = actorSystem.actorOf(Props[ReplicatedCache])

    val server = new WebServerServer(replicatedCache)
    server.startServer("localhost",8080)
  }
}

final case class Item(name: String, id: Long)

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val postImplicit = jsonFormat1(Posts) // contains List[Item]

}
