package david.webserver

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.ActorMaterializer
import akka.util.{ByteString, Timeout}
import david.webserver.ReplicatedCache.PutInCache
import org.scalatest.{Matchers, WordSpec}




//class WebServerSpec extends WordSpec with Matchers with ScalatestRouteTest  {
//  val  replicatedCacheVal= system.actorOf(Props[ReplicatedCache])
//  val  acSystem = system
//  val  routeClass = new RestRoute {
//    override val replicatedCache: ActorRef = replicatedCacheVal
//    override implicit val system: ActorSystem = acSystem
//    override implicit val materializer: ActorMaterializer = ActorMaterializer()
//  }
//  "The service" should {
//
//    "return a greeting for GET requests to the root path" in {
//      // tests:
//
//      Get() ~> routeClass.route ~> check {
//        responseAs[String] shouldEqual "Captain on the bridge!"
//      }
//    }
//
//    "return a 'json!' response for POST requests to /post" in {
//
//      val data = s"""
//                    |{
//                    |    "name":"test"
//                    |}
//        """.stripMargin
//      val jsonRequest = ByteString(data)
//      import akka.pattern.ask
//      import scala.concurrent.duration._
//      implicit val timeout = Timeout(5 seconds)
//
//      replicatedCacheVal ? PutInCache("key",data)
//      val postRequest = HttpRequest(
//        HttpMethods.POST,
//        uri = "/post",
//        entity = HttpEntity(MediaTypes.`application/json`, jsonRequest))
//
//
//      // tests:
//      postRequest ~> routeClass.route ~> check {
//        responseAs[String] shouldEqual data
//      }
//    }
//
//  }
//
//
//
//
//
//
//}
