name := "clustering"

organization := "com.davidho"

version := "0.3"


homepage := Some(url("https://github.com/mhamrah/clustering"))

startYear := Some(2017)


/* scala versions and options */
scalaVersion := "2.11.4"

// These options will be used for *all* versions.
scalacOptions ++= Seq(
  "-deprecation"
  ,"-unchecked"
  ,"-encoding", "UTF-8"
  ,"-Xlint"
  ,"-Yclosure-elim"
  ,"-Yinline"
  ,"-Xverify"
  ,"-feature"
  ,"-language:postfixOps"
)

val akkaV = "2.5.8"
val akkaHttp = "10.0.11"

/* dependencies */
libraryDependencies ++= Seq (
  "com.github.nscala-time" %% "nscala-time" % "1.2.0"
  // -- testing --
  , "org.scalatest" %% "scalatest" % "2.2.6" % Test
  // -- Logging --
  ,"ch.qos.logback" % "logback-classic" % "1.1.1"
  // -- Akka --
  ,"com.typesafe.akka" %% "akka-testkit" % akkaV
  ,"com.typesafe.akka" %% "akka-actor" % akkaV
  ,"com.typesafe.akka" %% "akka-slf4j" % akkaV
  ,"com.typesafe.akka" %% "akka-cluster" % akkaV,
  "com.typesafe.akka" %% "akka-stream" % akkaV,
  "com.typesafe.akka" %% "akka-distributed-data" % akkaV,
  "com.typesafe.akka" %% "akka-multi-node-testkit" % akkaV % "test"
  // -- json --
  ,"org.json4s" %% "json4s-jackson" % "3.2.10"
  // -- config --
  ,"com.typesafe" % "config" % "1.2.0",
  "com.typesafe.akka" %% "akka-http"   % akkaHttp,
  "com.typesafe.akka" %% "akka-http-spray-json"  % akkaHttp,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttp % "test"


)

lazy val root = (project in file("."))
  .enablePlugins(MultiJvmPlugin)
  .configs(MultiJvm)

maintainer := "David Hodeffi"

dockerExposedPorts in Docker := Seq(1600)

dockerEntrypoint in Docker := Seq("sh", "-c", "CLUSTER_IP=`/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1 }'` bin/clustering $*")

dockerRepository := Some("mhamrah")

dockerBaseImage := "java"
enablePlugins(JavaAppPackaging)
